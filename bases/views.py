from multiprocessing import context
from re import template
from django.shortcuts import redirect, render
from django.views.generic import *
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.auth.models import Group, Permission
from django.contrib.auth.hashers import make_password
from django.contrib.auth.decorators import login_required, permission_required
from django.db.models import Q

from .models import Usuario
from .forms import UserForm

class Home(LoginRequiredMixin, TemplateView):
    template_name = 'base/home.html'
    login_url = 'config:login'


class UserList(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    template_name = 'base/users_list.html'
    login_url = 'config:login'
    model = Usuario
    permission_required = 'bases:view_usuario'
    context_object_name = 'obj'

@login_required(login_url="config:login")
@permission_required('bases.change_usuario', login_url="config:home")
def user_admin(request, pk=None):
    template_name = 'base/users_add.html'
    context = {}
    form = None
    obj = None

    if request.method == 'GET':
        if not pk:
            form = UserForm(instance=None)
        else:
            obj = Usuario.objects.filter(id=pk).first()
            form = UserForm(instance=obj)
        context['form'] = form
        context['obj'] = obj
    
    if request.method == 'POST':
        data = request.POST
        e = data.get("email")
        fn = data.get("first_name")
        ln = data.get("last_name")
        p = data.get("password")

        if pk:
            obj = Usuario.objects.filter(id=pk).first()
            if not obj:
                print("Error Usuario No Existe")
            else:
                obj.email = e
                obj.first_name = fn
                obj.last_name = ln
                obj.password = make_password(p)
                obj.save()
        else:
            obj = Usuario.objects.create_user(
                email = e,
                password = p,
                first_name = fn,
                last_name = ln
            )
            print(obj.email, obj.password)

        return redirect('config:user_list')

    return render(request, template_name, context)


class UserGroupList(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    template_name = "base/users_group_list.html"
    login_url = "config:login"
    model = Group
    permission_required = "bases:view_usuario"
    context_object_name = "obj"

@login_required(login_url="config:login")
@permission_required("bases.change_usuario", login_url="bases:login")
def user_groups_admin(request, pk=None):
    template_name="base/users_group_add.html"
    context = {}

    obj = Group.objects.filter(id=pk).first()
    context["obj"] = obj
    permisos = {}
    permisos_grupo = {}
    context["permisos"] = permisos
    context["permisos_grupo"] = permisos_grupo

    if obj:
        permisos_grupo = obj.permissions.all()
        context["permisos_grupo"] = permisos_grupo

        permisos = Permission.objects.filter(~Q(group=obj))
        context["permisos"] = permisos

    if request.method == 'POST':
        name = request.POST.get("name")
        grp = Group.objects.filter(name=name).first()

        if grp and grp.id != pk:
            return redirect("config:user_group_new")

        if not grp and pk != None:
            # Grupo existe, se esta cambiando el Nombre
            grp = Group.objects.filter(id=pk).first()
            grp.name = name
            grp.save()
        
        elif not grp and pk == None:
            grp = Group(name=name)
        
        else:
            ...

        grp.save()
        return redirect("config:user_group_modify", grp.id)

    return render(request, template_name, context)
